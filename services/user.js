var User = require('../models/user');
var bcrypt = require('bcryptjs');
var fsPromises = require('fs').promises;
var saltRounds = 10;
const { Storage } = require('@google-cloud/storage');
var mongoose = require('mongoose');


const storage = new Storage();

class UserService {
    /**
     * Récupère un utilisateur par son Id
     * @param {ObjectId} id 
     */
    static getUserById(id) {
        return User.findById(id).exec();
    }

    /**
     * Récupère un utilisateur par son adresse mail
     * @param {string} mail 
     */
    static getUserByMail(mail) {
        return User.findOne({
            mail
        }).exec();
    }

    /**
     * Ajoute un utilisateur
     * @param {string} firstName prénom 
     * @param {string} lastName nom
     * @param {string} mail adresse mail (ne doit pas déjà exister, sinon l'opération échoue)
     * @param {string} password le mot de passe en clair
     * @param {string} phoneNumber numéro de téléphone
     */
    static async addUser(firstName, lastName, mail, password, phoneNumber) {
        return User({
            firstName,
            lastName,
            mail,
            password: await bcrypt.hash(password, saltRounds),
            phoneNumber,
            credit: 0,
            profilePicName: 'TODO',
            ratings: [],
            image: 'empty',
        }).save();
    }

    static async addOrUpdateRating(user, rater, note, rating) {
        const session = await User.startSession();
        session.startTransaction();
        try {
            const opts = { session };

            var userFound = await User.findById(user._id).session(session).exec();

            if (!userFound) throw new Error("user not found");

            var updateOperations = {};

            if (userFound.ratings.some(e => e._id === rater._id)) {
                // Pull old rating
                updateOperations.$pull = {
                    ratings: { from: rater },
                };
            }

            updateOperations.$push = {
                ratings: {
                    from: rater,
                    note,
                    rating
                },
            };

            await User.updateOne({
                _id: user._id,
            }, updateOperations, opts);

            await session.commitTransaction();
            session.endSession();
            return true;
        } catch (error) {
            // If an error occurred, abort the whole transaction and
            // undo any changes that might have happened
            await session.abortTransaction();
            session.endSession();
            throw error;
        }
    }

    static updateUser(user, firstName, lastName, phoneNumber, mail) {
        return User.findOneAndUpdate({
            _id: user._id
        }, {
            firstName,
            lastName,
            phoneNumber,
            mail
        }, {
            new: true
        });
    }

    static deleteUser(user) {
        // TODO: remove all travel and cars and ratings
        // Only allow if credit == 0 AND no current travel
    }

    /**
     * Vérifie si le mot de passe fourni en paramètre est correct
     * 
     * @param {User} user l'utilisateur
     * @param {string} password un mot de passe en clair
     */
    static checkPassword(user, password) {
        return bcrypt.compare(password, user.password);
    }

    static async updatePassword(user, password) {
        return User.updateOne({
            _id: user._id,
        }, {
            password: await bcrypt.hash(password, saltRounds),
        });
    }

    static addCredit(user, amount) {
        return User.findOneAndUpdate({
            _id: user._id
        }, {
            $inc: { credit: Number.parseFloat(amount) },
        }, {
            new: true
        });
    }

    static async updateProfilePic(user, file) {

        storage.bucket('travelexpress').file('image1/jpg').


        return;


        try {
            
            var bucketName = 'travelexpress';

            const bucket = storage.bucket(bucketName);

            var filename;
            var words = file.name.split('.');
            var extension = '.'+ words[words.length - 1];

            if (user.image === 'empty') {
                filename = new mongoose.Types.ObjectId().toString() + extension;
                
            } else {
                filename = user.image.split('.')[0] + extension;
                await bucket.file(user.image).delete();
            }

            //await fsPromises.writeFile('temp/' + filename, file);

            await file.mv('temp/'+filename);

            
            await bucket.upload('temp/'+filename,{
                public: true,
                destination : filename,

            });

            await fsPromises.unlink('temp/' + filename);

            //await bucket.file(filename).makePublic();

            await User.updateOne({_id: user._id,}, {image: filename});

            return true;

        } catch (err) {
            console.error('ERROR:', err);
            return false;
        }
    }
}

module.exports = UserService;