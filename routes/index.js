var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('home', { title: 'TravelExpress - Leader du covoiturage en France', auth: req.auth });
});

module.exports = router;
