var express = require('express');
var router = express.Router();
const { body, param, validationResult } = require('express-validator');
var denyAnonymousMiddleware = require('../middlewares/deny-anonymous');
var CarService = require('../services/car');
var ObjectId = require('mongoose').Types.ObjectId;
var CityService = require('../services/city');
var TravelService = require('../services/travel');
var createError = require('http-errors');

router.get('/add', denyAnonymousMiddleware, async function (req, res, next) {
  try {
    var cars = await CarService.getCars(req.user);

    if (cars.length === 0) return res.redirect('/profile/cars?err=1');

    res.render('newTravel', { title: 'Express', auth: req.auth, cars, req });

  } catch (error) {
    console.error(error);
    next(error);
  }
});

router.post('/add', denyAnonymousMiddleware, [
  body('addressDeparture')
    .exists({ checkFalsy: true })
    .withMessage("Veuillez indiquer l'adresse de départ")
    .isLength({ max: 300 })
    .withMessage("L'adresse de départ ne doit pas excéder 300 caractères"),
  body('addressArrival')
    .exists({ checkFalsy: true })
    .withMessage("Veuillez indiquer l'adresse d'arrivée")
    .isLength({ max: 300 })
    .withMessage("L'adresse d'arrivée ne doit pas excéder 300 caractères"),
  body('indications')
    .isLength({ max: 1000 })
    .withMessage("Les indications ne doivent pas excéder 1000 caractères"),
  body('departureTime')
    .exists({ checkFalsy: true })
    .withMessage("Veuillez indiquer l'heure de départ")
    .bail()
    .custom((value) => {
      TravelService.setTimeFromString(new Date(), value);
      return true;
    })
    .withMessage("L'heure de départ est incorrecte"),
  body('departureDate')
    .exists({ checkFalsy: true })
    .withMessage("Veuillez indiquer la date de départ")
    .custom((value) => TravelService.stringToDate(value))
    .bail()
    .custom((value) => {
      var today = new Date();
      today.setHours(0, 0, 0, 0);
      return TravelService.stringToDate(value) >= today;
    })
    .withMessage("Vous ne pouvez pas entrer une date dans le passé"),
  body('pricePerPassenger')
    .exists({ checkFalsy: true })
    .withMessage("Veuillez indiquer un prix (le prix ne peut pas être égal à 0€)")
    .isNumeric()
    .withMessage("Le prix doit être une valeur numérique"),
  body('places')
    .exists({ checkFalsy: true })
    .withMessage("Veuillez indiquer le nombre de places (ce nombre doit être suppérieur à 0)"),
  body('vehicule-selected')
    .exists({ checkFalsy: true })
    .withMessage("Veuillez selectionner un véhicule")
    .custom((value) => ObjectId.isValid(value))
    .withMessage("ID véhicule invalide"),
], async function (req, res, next) {
  try {
    var cars = await CarService.getCars(req.user);

    if (cars.length === 0) return res.redirect('/profile/cars?err=1');

    var errors = validationResult(req);

    // Check car
    var car = null;

    if (!("vehicule-selected" in errors.mapped())) {
      car = await CarService.getCarById(req.user, req.body["vehicule-selected"]);

      await body('vehicule-selected')
        .custom(() => car !== null)
        .withMessage("Le véhicule selectionné n'existe pas")
        .run(req);
    }

    // Check city departure & arrival
    var departure = await CityService.getCityByName(req.body.departure);
    var arrival = await CityService.getCityByName(req.body.arrival);

    console.log(departure);
    await body('departure')
      .custom(() => departure !== null)
      .withMessage("La ville de départ n'a pas été trouvée")
      .run(req);

    await body('arrival')
      .custom(() => arrival !== null)
      .withMessage("La ville d'arrivée n'a pas été trouvée")
      .run(req);

    errors = validationResult(req);

    if (errors.isEmpty()) {
      // Parse date and time
      var date = TravelService.stringToDate(req.body.departureDate);
      date = TravelService.setTimeFromString(date, req.body.departureTime);

      // Add
      var travel = await TravelService.addTravel(req.user, departure, req.body.addressDeparture, arrival, req.body.addressArrival, req.body.indications, date, req.body.pricePerPassenger, req.body.places, car);

      // Redirect
      res.redirect('/travel/' + travel._id);
    } else {
      console.log(errors);
      // Show errors
      res.render('newTravel', { title: 'Express', auth: req.auth, cars, errors: errors.array({ onlyFirstError: true }), req });
    }

  } catch (error) {
    console.error(error);
    next(error);
  }
});

/* GET home page. */
router.get('/:id', [
  param('id')
    .custom((value) => ObjectId.isValid(value))
    .withMessage("ID covoiturage invalide"),
], async function (req, res, next) {
  try {
    var errors = validationResult(req);

    if (!errors.isEmpty()) {
      return next(createError(404));
    }

    var travel = await TravelService.getTravelById(req.params.id);

    if (!travel) {
      return next(createError(404));
    }

    var reserved = 0;
    var owner = false;

    // Departure time
    var departureTime = travel.departureTime.toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1");

    // Calcul places restantes et check si déjà réservé
    var placesLeft = travel.places;
    travel.passengers.forEach(function (passenger) {
      placesLeft -= passenger.places;
      if (req.user && req.user._id.toString() == passenger.user._id.toString()) reserved = passenger.places;
    });

    // Check si proprio annonce
    if (req.user && travel.host._id.toString() == req.user._id.toString()) owner = true;

    res.render('travel', {
      title: 'Express',
      auth: req.auth,
      travel,
      frenchDate: TravelService.frenchDateString(travel.departureTime),
      departureTime: departureTime.substring(0, 5),
      placesLeft,
      reserved,
      owner
    });
  } catch (error) {
    console.error(error);
    next(error);
  }
});

router.post('/:id', [
  param('id')
    .custom((value) => ObjectId.isValid(value))
    .withMessage("ID covoiturage invalide"),
  body('action')
    .exists({ checkFalsy: null })
    .withMessage("Veuillez indiquer une action")
    .isIn(['delete', 'add-passenger', 'remove-passenger', 'travel-done'])
    .withMessage("Action invalide")
], denyAnonymousMiddleware, async function (req, res, next) {
  try {
    var errors = validationResult(req);

    if (!errors.isEmpty()) {
      return next(createError(404));
    }

    var travel = await TravelService.getTravelById(req.params.id);

    if (!travel) {
      return next(createError(404));
    }

    // Can't modify travel done
    if (travel.travelDone) {
      return next(createError(404));
    }

    var reserved = false;
    travel.passengers.forEach(function (passenger) {
      if (req.user && req.user._id.toString() == passenger.user._id.toString()) reserved = true;
    });

    if (req.user._id.toString() === travel.host._id.toString() && req.body.action === 'delete') {
      // Delete travel (will refund all passengers)
      await TravelService.cancelTravel(req.user, travel);
      return res.redirect('/');

    } else if (req.user._id.toString() === travel.host._id.toString() && req.body.action === 'travel-done') {
      await TravelService.endTravel(req.user, travel);
    }
    else if (reserved && req.body.action === 'remove-passenger') {
      // Set passenger to 0 (-> Will remove passenger)
      await TravelService.addOrUpdatePassenger(req.user, travel, 0);

    } else if (!reserved && req.body.action === 'add-passenger') {
      //TODO:check credit before calling the function!
      await TravelService.addOrUpdatePassenger(req.user, travel, Number.parseInt(req.body.places));
    }

    res.redirect('/travel/' + req.params.id);
  } catch (error) {
    console.error(error);
    next(error);
  }


});


module.exports = router;
