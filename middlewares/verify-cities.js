var CityService = require('../services/city');

/**
 * Vérifie que la ville "Rennes (35000)" existe dans la base de données
 * On part du principe que si elle n'est pas présente, la base de données
 * ne contient aucune villes et donc le site ne peut pas fonctionner correctement...
 */
module.exports = async function(_res, _req, next) {
    if(await CityService.importFromCsvIfNecessary(__dirname + '/villes_france.csv')) {
        next(new Error("Database not initialized yet... Please wait"));
    } else {
        next();
    }
};
