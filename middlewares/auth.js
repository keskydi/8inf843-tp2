var UserService = require('../services/user');

/**
 * Détermine si l'utilisateur est authentifié ou non
 * 
 * @param {*} req 
 * @param {*} _res 
 * @param {*} next 
 */
async function authMiddleware(req, _res, next) {
    try {
        if(req.session.userId) {
            req.user = await UserService.getUserById(req.session.userId);
            req.auth = true;
        } else {
            req.user = null;
            req.auth = false;
        }

        next();
    } catch(error) {
        console.error(error);
        next(error);
    }
}

module.exports = authMiddleware;