/**
 * Interdit l'accès aux utilisateurs non authentifiés
 */
module.exports = function(req, res, next) {
    if(!req.auth) {
        return res.redirect('/');
    } else {
        next();
    }
}