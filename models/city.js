const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const citySchema = Schema({
    name : { type: String, required: true },
    postalCode: { type: String, required: true },
    nameWithPostalCode: { type: String, required: true },
});

const City = mongoose.model('City', citySchema);

module.exports = City;
