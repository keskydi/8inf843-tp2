const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const carSchema = Schema({
    model: { type: String, required: true },
    plateNumber: { type: String, required: true },
    luggageSize: { type: Number, required: true },
    smoke: { type: Boolean, required: true },
    animals: { type: Boolean, required: true },
    clim: { type: Boolean, required: true },
    bike: { type: Boolean, required: true },
    owner: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
});

const Car = mongoose.model('Car', carSchema);

module.exports = Car;
